package com.app.linkedlist;

public class Main {
    public static void main(String[] args) {
        MyLinkedList<Integer> list = new MyLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);
        list.append(4);
        list.append(5);
        list.append(6);
        list.append(7);

        for(Integer number: list){
            System.out.println(number);
        }

        System.out.println();

        System.out.println(list.getMiddleNode());

        System.out.println("Size of list: " + list.getSize());

    }
}
