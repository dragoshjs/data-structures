package com.app.linkedlist;

import java.util.Iterator;

public class MyLinkedList<T> implements Iterable<T> {

    private int size = 0;

    private Node<T> first;
    private Node<T> last;

    public void append(T value){
        if (first == null){
            Node<T> node = new Node<>();
            node.setData(value);
            first = node;
            last = node;
            size++;
        }else{
            Node<T> node = new Node<>();
            node.setData(value);
            last.setNext(node);
            last = node;
            size++;
        }
    }

    public T getMiddleNode(){

        if(first == null){
            return null;
        }

        int length = 1;
        Node<T> node = first;
        Node<T> middle = first;

        while (node.getNext() != null){

            length++;

            if(length % 2 == 1){
                middle = middle.getNext();
            }
            node = node.getNext();
        }

        if(length % 2 == 0){
            middle = middle.getNext();
        }

        System.out.println("length of LinkedList: " + length);
        System.out.println("Middle element of LinkedList : " + middle.getData());

        return middle.getData();
    }

    public int getSize(){
        return size;
    }

    public Iterator<T> iterator() {
        return new NodeIterator<T>(first);
    }

}
