package com.app.binarytree;

class Node<T> implements Comparable<Integer>{
    Integer key;
    T value;
    Node<T> left;
    Node<T> right;

    Node(int key, T value){
        this.key = key;
        this.value = value;
    }

    @Override
    public int compareTo(Integer key) {
        if (this.key.equals(key))
            return 0;
        else
            if (this.key > key)
                return 1;
            else
                return -1;
    }
}
