package com.app.binarytree;

public class BinaryTree<T> {
    private Node<T> root ;

    public BinaryTree(){
        root = null;
    }

    public T get (int k){
        Node<T> x = root;
        while (x != null) {
            int cmp = x.compareTo(k);
            if (cmp == 0){
                return  x.value;
            }
            if (cmp < 0){
                x = x.left;
            } else {
                x = x.right;
            }
        }
        return null;
    }

    public void add(int key, T value) {
        Node<T> copyRoot = root;
        Node<T> node = null;

        while (copyRoot != null) {
            int cmp = copyRoot.compareTo(key);
            if (cmp == 0) {
                copyRoot.value = value;
                return;
            } else {
                node = copyRoot;
                if (cmp < 0) {
                    copyRoot = copyRoot.left;
                } else {
                    copyRoot = copyRoot.right;
                }
            }
        }

        Node<T> newNode = new Node<>(key, value);
        if (node == null) {
            root  = newNode;
        } else {
            if (node.compareTo(key) < 0) {
                node.left = newNode;
            } else {
                node.right = newNode;
            }
        }
    }

    public void remove(int key) {
        Node<T> copyRoot = root;
        Node<T> node = null;
        while (copyRoot != null) {
            int cmp = copyRoot.compareTo(key);
            if(cmp == 0){
                break;
            } else {
                node = copyRoot;
                if (cmp < 0) {
                    copyRoot = copyRoot.left;
                } else {
                    copyRoot = copyRoot.right;
                }
            }
        }
        if (copyRoot == null) {
            return;
        }
        if (copyRoot.right == null) {
            if(node == null) {
                root = copyRoot.left;
            } else {
                if (copyRoot == node.left) {
                    node.left = copyRoot.left;
                } else {
                    node.right = copyRoot.left;
                }
            }
        } else {
            Node<T> leftMost = copyRoot.right;
            node = null;
            while (leftMost.left != null){
                node = leftMost;
                leftMost = leftMost.left;
            }

            if (node != null) {
                node.left = leftMost.right;
            } else {
                copyRoot.right = leftMost.right;
            }
            copyRoot.key = leftMost.key;
            copyRoot.value = leftMost.value;
        }
    }
}
