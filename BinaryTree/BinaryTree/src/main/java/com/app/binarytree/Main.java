package com.app.binarytree;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        BinaryTree<Integer> tree = new BinaryTree<>();
        System.out.println("Добавляем записи...");
        tree.add(1, 22);
        tree.add(3, 0);
        tree.add(0, -1);
        tree.add(3, 8);
        System.out.println("Вывод записей: ");
        System.out.println("Key: 1 - Value: " + tree.get(1));
        System.out.println("Key: 3 - Value: " + tree.get(3));
        System.out.println("Key: 0 - Value: " + tree.get(0));
        System.out.println("Удаляем запись с ключом 3...");
        tree.remove(3);
        System.out.println("Вывод записей после удаления: ");
        System.out.println("Key: 1 - Value: " + tree.get(1));
        System.out.println("Key: 3 - Value: " + tree.get(3));
        System.out.println("Key: 0 - Value: " + tree.get(0));
    }
}
