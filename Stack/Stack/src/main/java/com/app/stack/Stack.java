package com.app.stack;

import java.util.EmptyStackException;

public class Stack{

    private Object[] array;
    private int size = 0;

    public Stack(){
        this.array =  new Object[0];
    }

    public void push(Object value){
        ensureCapacity();
        array[size++] = value;
    }

    public Object pop(){
        if (size == 0)
            throw new EmptyStackException();
        Object result = array[--size];
        array[size] = null;
        return result;
    }

    public void ensureCapacity(){
        if(size == array.length) {
            Object[] oldArray = array;
            array = new Object[array.length + 1];
            System.arraycopy(oldArray, 0, array, 0, oldArray.length);
        }
    }

    public int getSize(){
            return array.length;
    }


}
