package com.app.doublylinkedlist;

public class Main {
    public static void main(String[] args) {
        MyDoublyLinkedList<Integer> list = new MyDoublyLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);
        list.append(4);
        list.append(5);

        for(Integer item: list){
            System.out.println(item);
        }

        System.out.println("\nSize of list before remove last node: " + list.getSize());

        list.remove();
        System.out.println("Size of list after remove last node: " + list.getSize() + "\n");

        for(Integer item: list){
            System.out.println(item);
        }
    }
}
