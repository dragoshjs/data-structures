package com.app.doublylinkedlist;

import java.util.Iterator;
import java.util.Optional;

class NodeIterator<T> implements Iterator<T> {
    private Node<T> current;
    private Node<T> first;

    public NodeIterator(Node<T> first){
        this.first = first;
    }
    @Override
    public boolean hasNext() {
        if (current == null){
            current = first;
            return Optional.ofNullable(current).isPresent();
        }else{
            current = current.getNext();
            return Optional.ofNullable(current).isPresent();
        }
    }

    @Override
    public T next() {
        return current.getData();
    }
}
