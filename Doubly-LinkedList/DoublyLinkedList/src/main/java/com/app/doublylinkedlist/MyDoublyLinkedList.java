package com.app.doublylinkedlist;

import java.util.Iterator;

public class MyDoublyLinkedList<T> implements Iterable<T> {

    private int size;
    private Node<T> first;
    private Node<T> last;

    public MyDoublyLinkedList(){
        first = null;
        last = null;
        size = 0;
    }

    public void append(T value){
        if (first == null){
           Node<T> node = new Node<>();
           node.setData(value);
           first = node;
           last = node;
           size++;
        }else{
            Node<T> node = new Node<>();
            node.setData(value);
            last.setNext(node);
            node.setPrev(last);
            last = node;
            size++;
        }
    }

    public void remove(){
        if (isEmpty())
            return;
        last = last.getPrev();
        last.setNext(null);
        size--;
    }

    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new NodeIterator<T>(first);
    }
}
