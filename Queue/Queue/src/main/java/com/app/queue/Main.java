package com.app.queue;

public class Main {
    public static void main(String[] args) {
        MyQueue queue = new MyQueue();
        queue.push(1);
        queue.push(2);
        queue.push(3);
        queue.push(4);
        queue.push(5);
        queue.push(6);
        queue.push(7);
        queue.push(8);

        System.out.println("Length of the queue: " + queue.getLength());

        for(; !queue.isEmpty();){
            System.out.println(queue.pop());
        }

        queue.pop();

    }
}
