package com.app.queue;

public class MyQueue {

    private int size;
    private Object[] array;

    public MyQueue(){
        array = new Object[1];
        size = 0;
    }

    public void push(Object data){
        ensureCapacity();
        array[size++] = data;
    }

    public Object pop(){
        if (!isEmpty()) {
            Object data = array[0];
            array[0] = null;
            reduceCapacity();
            return data;
        } else
            throw new NullPointerException();

    }

    public int getLength(){
        return array.length;
    }

    public boolean isEmpty(){
        return array.length == 0;
    }

    private void ensureCapacity(){
        if (size == array.length){
            Object[] oldArray = array;
            array = new Object[oldArray.length + 1];
            System.arraycopy(oldArray,0,array,0,oldArray.length);
        }
    }

    private void reduceCapacity(){
        Object[] oldArray = array;
        array = new Object[oldArray.length-1];
        for (int i = 0; i < array.length; i++){
            array[i] = oldArray[i + 1];
        }
    }


}
